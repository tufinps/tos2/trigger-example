#!/bin/bash
rm -rf ./resources.tar.gz

if [[ "$(docker images -q myimage:mytag 2> /dev/null)" == "" ]]; then
  echo trigger_build does not exist, building...
  docker build -t trigger_build .
fi


docker run --rm -dit --name trigger_builder trigger_build bash
docker cp ../pyproject.toml trigger_builder:/resources/
docker cp ../poetry.lock trigger_builder:/resources/
docker cp ../mediator.sh trigger_builder:/resources/
docker cp ../trigger_proxy/ trigger_builder:/resources/trigger_proxy/
docker exec trigger_builder sh -c 'cd resources && \
    poetry export --format requirements.txt --without-hashes > requirements.txt && \
    pip3 download -r requirements.txt -d wheels && \
    tar zcvf resources.tar.gz --exclude=*__pycache__ --exclude=resources.tar.gz ./*'
docker cp trigger_builder:/resources/resources.tar.gz .
docker rm -f trigger_builder

cat ./self-extract.sh ./resources.tar.gz > trigger_proxy.install

rm -rf resources.tar.gz
chmod +x ./trigger_proxy.install
