#!/bin/bash
read -p "trigger-service will be installed in the current directory, OK [y/n]? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Nn]$ ]]
then
    echo "Installation aborted"
    exit 1
fi

export TMPDIR=`mktemp -d /tmp/trigger_proxy.XXXXXX`

ARCHIVE=`awk '/^__ARCHIVE__/ {print NR + 1; exit 0; }' $0`

#tail -n+$ARCHIVE $0 | tar xzv -C $TMPDIR
mkdir trigger-service
tail -n+$ARCHIVE $0 | tar xzv -C trigger-service

CDIR=`pwd`
# cd $TMPDIR
cd trigger-service
python3 -m venv venv
source venv/bin/activate
pip3 install wheels/* --no-index
echo "Run the following command to start the web service"
echo -e "\e[3mtrigger-service/venv/bin/uvicorn --app-dir trigger-service trigger_proxy:app --reload --host 0.0.0.0\e[0m"

cd $CDIR
rm -rf $TMPDIR

exit 0

__ARCHIVE__
