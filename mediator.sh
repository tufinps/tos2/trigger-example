#!/bin/bash
TICKET_INFO=`cat | base64 -w0`
# Replace this with the externally accessible IP of the TOS host
SCRIPT_HOST=192.168.40.73:8000

resp=$(curl -sS --fail -X POST -u "user:pass"  -H "Content-Type: application/json" "$SCRIPT_HOST/trigger_file" -d \
    "{ \
       \"ticket_info\":\"$TICKET_INFO\", \
       \"args\":\"$2\", \
       \"event\":\"$SCW_EVENT\", \
       \"script\":\"$1\" \
     }" 2>&1)

ret=$?
if [ $ret -ne 0 ]; then
    echo $resp
    exit $ret
fi
echo $resp | python -c "import sys, json
out = json.load(sys.stdin)
if out['error']:
    print out['error']['output'],
    sys.exit(out['error']['returnCode'])
else:
    print out['output'],
"
