## Aurora SecureChange Custom Scripting Solution

### Introduction

See the Tufin KC for detailed information on [Enabling Custom Scripts](https://forum.tufin.com/support/kc/latest/Content/Suite/SC-CustomScripts.htm?cshid=mediator)

This repo contains an example trigger solution for SecureChange that allows for file based triggers to be used as is with Aurora, where files and runtime are harder to manage than on TOS classic. 

The solution contains two pieces:
- The mediator script which acts as the "file" that SecureChange triggers and proxies the request
- The web service which runs outside of the Aurora cluster and runs the file based trigger (or python code)

### Requirements for TufinOS3 and TufinOS4 (up to 4.20)
This project requires:
- python ~3.6
- [poetry](https://python-poetry.org/) ~1.0.0

### Requirements for Non TufinOS or other python versions
   - [poetry](https://python-poetry.org/) ~1.0.0
   - python 3.7 to 3.12
      - Modify the pyproject.toml file to your required version based on [Poetry Dependency specification](https://python-poetry.org/docs/dependency-specification/)
      - Examples: 
      ```toml
      [tool.poetry.dependencies]
      # Example 1
      python = "~3.11.0"  # any Python 3.11.x 
      # Example 2
      python = "^3" # any Python 3
      ```
   - Python 3.7+ ships with the Dataclasses in the standard library, and will need to be removed from the dependencies in pyproject.toml

	 ```toml
	 # dataclasses = "^0.8"

	 ```
   - Optional: Update project dependencies to meet own requirements with `poetry update`

## Getting Started

1. Clone this repo, or copy the tar.gz archive to the cluster host
   ```console
	 git clone git@gitlab.com:tufinps/tos2/trigger-example.git
	```

	or

  ```console
	mkdir -p trigger-service && curl "https://gitlab.com/api/v4/projects/tufinps%2Ftos2%2Ftrigger-example/repository/archive.tar.gz" | tar -zx -C trigger-service --strip-components 1
  ```

2. Install the necessary requirements
   ```console
   cd trigger-example && \
     python3 -m venv venv && \
     source venv/bin/activate && \
     pip3 install --upgrade pip && \
     pip3 install poetry && \
     poetry install
   ```

3. Start the web service (ideally in screen)
   ```console
	 poetry run uvicorn trigger_proxy:app --reload
	
	```

### Authentication

This project supports basic auth and IP based authentication

#### IP Based Authentication

IP based authentication validates that the source IP of any request is the sc-server pod. This prevents any other hosts or pods from communicating with the trigger web service. To use IP based auth on a host other than the TOS2 cluster host, Kubernetes API access must be configured using [standard practices](https://kubernetes.io/docs/concepts/configuration/organize-cluster-access-kubeconfig/)


#### Basic Authentication
Update [trigger_proxy/\_\_init__.py](trigger_proxy/__init__.py) to use the `basic_auth` dependency instead of `ip-security`

Update the auth in [mediator.sh](mediator.sh) the curl command to match the [web server](trigger_proxy/__init__.py)

### Offline installation

#### Requirements

1. POSIX shell to run the script - Linux, Mac or Linux subsystem for Windows will work
2. docker - The script needs access to a running docker daemon to download the python dependencies

#### Installer Build process

To build the offline installer, run `build.sh` in the _install_ directory

```console
cd installer
./build.sh
```

This will produce the installer _trigger_proxy.install_

scp this to the TufinOS3 host, and then install with

```console
./trigger_proxy.install
```

Following the script output from there.

### Basic Troubleshooting 


#### Additional debugging of the server process.

Enable additional server side output by increasing the [log level of uvicorn](https://www.uvicorn.org/settings/#logging)

```console 
poetry run uvicorn trigger_proxy:app --reload --host 0.0.0.0 --log-level trace
```

#### Testing API Response
Two example json files have been included in the test_files directory.

To test either trigger_proxy API endpoint response, use the following examples:


###### Valid JSON

```console
curl -v -u "user:pass" -H "Content-Type: application/json" http://PROXY_HOST_ADDR:8000/trigger_function -d @good-test.json
```

###### Invalid JSON

```console
curl -v -u "user:pass" -H "Content-Type: application/json" http://PROXY_HOST_ADDR:8000/trigger_function -d @bad-test.json
```


