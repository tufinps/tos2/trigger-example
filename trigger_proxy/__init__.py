__version__ = "0.1.1"

import asyncio
import os
from pathlib import Path
import secrets
import sys
from typing import Optional
from base64 import b64decode,binascii
from xml.etree.ElementTree import fromstring

from fastapi import FastAPI, Request
from fastapi import Depends, FastAPI, HTTPException
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from kubernetes import client, config
from pydantic import BaseModel, constr

app = FastAPI()


class Body(BaseModel):
    ticket_info: str
    event: str
    args: str = ""
    script: Path


class Error(BaseModel):
    returnCode: int = 0
    output: str


class ResponseBody(BaseModel):
    error: Optional[Error] = None
    output: str


security = HTTPBasic()


def safe_path(path):
    basedir = os.getcwd()
    return basedir == os.path.commonpath((basedir, os.path.realpath(path)))


def ip_security(request: Request):

    # kubernetes option must be installed

    gravity_kube_config = "/var/lib/gravity/kubectl.kubeconfig"
    if Path(gravity_kube_config).is_file():
        config.load_kube_config(gravity_kube_config)
    else:
        config.load_kube_config()

    v1 = client.CoreV1Api()

    pods = v1.list_namespaced_pod(namespace="default", label_selector="app=sc-server")

    ips = [item.status.pod_ip for item in pods.items]

    if request.client.host not in ips:
        raise HTTPException(401, detail="Access only permitted from the sc-server pod")


def basic_auth(credentials: HTTPBasicCredentials = Depends(security)):
    # CHANGE THESE CREDENTIALS TO MATCH THE CREDENTIALS SET IN mediator.sh
    correct_username = secrets.compare_digest(credentials.username, "username")
    correct_password = secrets.compare_digest(credentials.password, "password")
    if not (correct_username and correct_password):
        raise HTTPException(401, detail="Incorrect credentials")


@app.post("/test", dependencies=[Depends(ip_security)])
async def test():
    return 200


# Use the ip_security dependency to lock down access to the sc-server pod ONLY
@app.post("/trigger_file", dependencies=[Depends(ip_security)])
# @app.post("/trigger_file", dependencies=[Depends(basic_auth)])
async def trigger_file(body: Body, request: Request):
    if not safe_path(body.script):
        # Script path must be a subdirectory under the directory that the web service was started in
        raise HTTPException(401, detail="Script path not allowed")
    cmd = str(body.script)
    print(f"Executing command `SCW_EVENT={body.event} {cmd} {body.args}`")
    # The argument will trigger a file-based script using subprocess, and set SCW_EVENT as the script expects
    # Uncomment the line below to hardcode the script command instead of using the script passed by the argument
    # cmd = "poetry run trigger_proxy/ticket_functions.py"
    #
    proc = await asyncio.create_subprocess_shell(
        # arguments are passed here
        f"SCW_EVENT={body.event} {cmd} {body.args}",
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
        stdin=asyncio.subprocess.PIPE,
    )
    # ticket_info is communicated via stdin just like securechange would if run directly
    try:
        ticket_xml = b64decode(body.ticket_info)
    except binascii.Error:
        raise HTTPException(400, detail="Unable to base64 decode ticket_info.")

    stdout, stderr = await proc.communicate(input=ticket_xml)
    # print the errors
    # print(stderr.decode())
    error = (
        {"returnCode": proc.returncode, "output": stderr.decode()}
        if proc.returncode and proc.returncode > 0
        else None
    )
    return {"error": error, "output": stdout.decode()}

# Use the ip_security dependency to lock down access to the sc-server pod ONLY
@app.post("/trigger_function", dependencies=[Depends(ip_security)])
# @app.post("/trigger_function", dependencies=[Depends(basic_auth)])
async def function_example(
    body: Body,
):
    """
    This is an example of a script that does not use an external script,
    and instead processes everything in Python under the web service process
    To use a function-based solution, the mediator script should be updated to send the request
    to this route function (/trigger_function)
    """
    if body.event == "ADVANCE":
        # Decode and Parse the ticket from the ticket_info xml
        try:
            ticket_info_xml = fromstring(b64decode(body.ticket_info))
        except binascii.Error:
            raise HTTPException(400, detail="Unable to base64 decode ticket_info.")

        # Parse the ticket id
        x = ticket_info_xml.find("id")
        ticket_id = x.text if x is not None else None

        # parse the previous step
        previous_stage = ticket_info_xml.find("current_stage")
        x = previous_stage.find("name")
        previous_step_name = (
            (x.text if x is not None else None) if previous_stage else None
        )

        # Run any customized code to here to interact with the ticket
        # For example, code to copy a field, or re-assigning a ticket
        # If this is a dynamic assignment trigger then the script must return a valid response

        return [ticket_id, previous_step_name]
