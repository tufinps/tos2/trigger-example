#!/usr/bin/env python3

import asyncio
from datetime import datetime
import json
from os import getenv
from sys import stdin
from xml.etree.ElementTree import fromstring

import httpx
from pydantic import AnyUrl, BaseSettings


class Settings(BaseSettings):
    securechange_url: AnyUrl = "https://user:password@localhost/securechangeworkflow/api/securechange"  # type: ignore

    class Config:
        # .env file overrides
        env_file = ".env"
        env_file_encoding = "utf-8"


settings = Settings()


async def get_ticket(ticket_id):
    async with httpx.AsyncClient(
        base_url=settings.securechange_url, verify=False, timeout=30
    ) as client:
        res = await client.get(f"/tickets/{ticket_id}.json")
        res.raise_for_status()
        ticket_json = res.json()
        return ticket_json


async def update_field(ticket, field_name, value):
    ticket_id = ticket["ticket"]["id"]
    current_step = ticket["ticket"]["steps"]["step"][-1]
    current_task = current_step["tasks"]["task"]
    x = [f for f in current_task["fields"]["field"] if f["name"] == field_name]
    field = x[0] if len(x) > 0 else None
    if not field:
        return None
    field["reason"] = value
    put_field_url = f"/tickets/{ticket_id}/steps/{current_step['id']}/tasks/{current_task['id']}/fields/"
    async with httpx.AsyncClient(
        base_url=settings.securechange_url,
        verify=False,
        timeout=30,
        headers={"Accept": "application/json"},
    ) as client:
        return await client.put(
            put_field_url,
            json={"fields": {"field": [field]}},
        )


async def process(ticket_id, previous_step):
    ticket = await get_ticket(ticket_id)
    if previous_step != ticket["ticket"]["steps"]["step"][-2]["name"]:
        print("Ticket is no longer at the expected step")
    res = await update_field(
        ticket,
        "Approve Access Request",
        "newval" + str(datetime.now()),
    )
    print(res.text if res else "no field match")
    if res and not res.is_error:
        print("Successfully updated field")
    return res


async def run_from_stdin():
    scw_event = getenv("SCW_EVENT")
    if scw_event == "ADVANCE":
        ticket_info = stdin.read()

        ticket_info_xml = fromstring(ticket_info)

        # Parse the ticket id
        x = ticket_info_xml.find("id")
        ticket_id = x.text if x is not None else None

        # parse the previous step
        previous_stage = ticket_info_xml.find("current_stage")
        x = previous_stage.find("name")
        previous_step_name = (
            (x.text if x is not None else None) if previous_stage else None
        )
        return await process(ticket_id, previous_step_name)


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run_from_stdin())
    # asyncio.run(run_from_stdin())
